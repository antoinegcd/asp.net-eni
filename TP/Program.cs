﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjetLinq.BO;

namespace TP
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            InitialiserDatas();
            foreach (Auteur auteur in ListeAuteurs.Where(a => a.Nom.StartsWith("G")))
            {
                Console.WriteLine(auteur.Prenom);
            }

            Auteur aut = ListeLivres.GroupBy(l => l.Auteur).OrderByDescending(l=>l.Count()).FirstOrDefault().Key;
            Console.WriteLine(aut.Nom + " " + aut.Prenom);
            int nbPageMoyen = 0;
            foreach (Livre livre in ListeLivres)
            {
                nbPageMoyen += livre.NbPages;
            }

            nbPageMoyen = nbPageMoyen / ListeLivres.Count;
            Console.WriteLine($"Nombre de page moyen par livre = {nbPageMoyen}");

            Livre livr = ListeLivres.OrderByDescending(p => p.NbPages).FirstOrDefault();
            Console.WriteLine(livr.Titre);
            
            decimal moyenneFacture = ListeAuteurs.Average(a => a.Factures.Sum(f => f.Montant));
            Console.WriteLine(moyenneFacture);
            
            var livresparAuteur2 = ListeLivres.GroupBy(l => l.Auteur);
            foreach (var livres in livresparAuteur2)
            {
                Console.WriteLine(livres.Key.Prenom + " " + livres.Key.Nom);
                foreach (var livre in livres)
                {
                    Console.WriteLine(livre.Titre);
                }
            }

            ListeLivres.Select(l => l.Titre).OrderBy(t => t).ToList().ForEach(Console.WriteLine);

            var moyennePages = ListeLivres.Average(l => l.NbPages);
            var livresPagesSupMoy = ListeLivres.Where(l => l.NbPages > moyennePages);
            foreach (var livre in livresPagesSupMoy)
            {
                Console.WriteLine(livre.Titre);
            }
            Console.WriteLine();

            var auteurMoinsDeLivres = ListeAuteurs.OrderBy(a => ListeLivres.Count(l => l.Auteur == a)).FirstOrDefault();
            Console.WriteLine($"{auteurMoinsDeLivres.Prenom} {auteurMoinsDeLivres.Nom}");

            Console.ReadKey();
        }

        private static List<Auteur> ListeAuteurs = new List<Auteur>();
        private static List<Livre> ListeLivres = new List<Livre>();

        private static void InitialiserDatas()
        {
            ListeAuteurs.Add(new Auteur("GROUSSARD", "Thierry"));
            ListeAuteurs.Add(new Auteur("GABILLAUD", "J�r�me"));
            ListeAuteurs.Add(new Auteur("HUGON", "J�r�me"));
            ListeAuteurs.Add(new Auteur("ALESSANDRI", "Olivier"));
            ListeAuteurs.Add(new Auteur("de QUAJOUX", "Benoit"));
            ListeLivres.Add(new Livre(1, "C# 4", "Les fondamentaux du langage", ListeAuteurs.ElementAt(0), 533));
            ListeLivres.Add(new Livre(2, "VB.NET", "Les fondamentaux du langage", ListeAuteurs.ElementAt(0), 539));
            ListeLivres.Add(new Livre(3, "SQL Server 2008", "SQL, Transact SQL", ListeAuteurs.ElementAt(1), 311));
            ListeLivres.Add(new Livre(4, "ASP.NET 4.0 et C#", "Sous visual studio 2010", ListeAuteurs.ElementAt(3), 544));
            ListeLivres.Add(new Livre(5, "C# 4", "D�veloppez des applications windows avec visual studio 2010", ListeAuteurs.ElementAt(2), 452));
            ListeLivres.Add(new Livre(6, "Java 7", "les fondamentaux du langage", ListeAuteurs.ElementAt(0), 416));
            ListeLivres.Add(new Livre(7, "SQL et Alg�bre relationnelle", "Notions de base", ListeAuteurs.ElementAt(1), 216));
            ListeAuteurs.ElementAt(0).addFacture(new Facture(3500, ListeAuteurs.ElementAt(0)));
            ListeAuteurs.ElementAt(0).addFacture(new Facture(3200, ListeAuteurs.ElementAt(0)));
            ListeAuteurs.ElementAt(1).addFacture(new Facture(4000, ListeAuteurs.ElementAt(1)));
            ListeAuteurs.ElementAt(2).addFacture(new Facture(4200, ListeAuteurs.ElementAt(2)));
            ListeAuteurs.ElementAt(3).addFacture(new Facture(3700, ListeAuteurs.ElementAt(3)));
        }
    }
}