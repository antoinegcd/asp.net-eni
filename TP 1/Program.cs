﻿using System;
using System.Collections.Generic;
using TP_1.classe;

namespace TP_1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            List<Forme> formes = new List<Forme>();
            formes.Add(new Cercle{Rayon = 3});
            formes.Add(new Rectangle{Largeur=3, Longueur=4});
            formes.Add(new Carre{Longueur = 3});
            formes.Add(new Triangle{A = 4, B = 5, C = 6});
            foreach (Forme forme in formes)
            {
                Console.WriteLine(forme);
            }

            Console.ReadKey();
        }
    }

}