using System;
using System.Security.Policy;

namespace TP_1.classe
{
    public class Triangle : Forme
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }
        private double hauteur => (Math.Pow(2, A) + Math.Pow(2,B));
        public override double aire { get; }
        public override double perimetre => A + B + C;
        public override string ToString()
        {
            return $"Triangle de côté A={A}, B={B}, C={C}" + Environment.NewLine + base.ToString();
        }
    }
}