using System;

namespace TP_1.classe
{
    public class Cercle : Forme
    {
        public int Rayon { get; set; }
        public override double aire => Math.PI * (Rayon * Rayon);
        public override double perimetre => 2 * Math.PI * Rayon;

        public override string ToString()
        {
            return $"Cercle de rayon = {Rayon}" + Environment.NewLine + base.ToString();
        }
    }
}