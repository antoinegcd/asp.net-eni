using System;

namespace TP_1.classe
{
    public class Rectangle : Forme
    {
        public virtual int Largeur { get; set; }
        public int Longueur { get; set; }
        public override double aire => Largeur * Longueur;
        public override double perimetre => Largeur * 2 + Longueur * 2;

        public override string ToString()
        {
            return ToString(false);
        }

        private string ToString(bool carre)
        {
            if (carre)
            {
                return base.ToString();
            }

            return $"Rectangle de longueur = {Longueur} et largeur = {Largeur}" + Environment.NewLine + base.ToString();
        }
    }
}