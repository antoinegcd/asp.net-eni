using System;

namespace TP_1.classe
{
    public class Carre : Rectangle
    {
        public override int Largeur => Longueur;
        public override string ToString()
        {
            return $"Carré de coté = {Largeur}" + Environment.NewLine + base.ToString();
        }
    }
}