using System;

namespace TP_1.classe
{
    public abstract class Forme
    {
        public abstract double aire { get; }
        public abstract double perimetre { get; }
        public override string ToString()
        {
            return $"Aire = {aire}" + Environment.NewLine + $"Périmètre = {perimetre}" + Environment.NewLine;
        }
    }
}